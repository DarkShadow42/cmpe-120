import threading
import time

start = time.perf_counter()


def thread_one_function():
    for i in range(10):
        print(i)
        time.sleep(1)


def thread_two_function():
    for i in range(10):
        print(10 - i)
        time.sleep(1)


t1 = threading.Thread(target=thread_one_function)
t2 = threading.Thread(target=thread_two_function)


t1.start()
t2.start()


t1.join()
t2.join()