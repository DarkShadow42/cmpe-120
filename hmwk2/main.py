def toUpperCase(a):
    if 97 <= ord(a) <= 122:
        a = chr(ord(a) ^ 32)
    return a


def toLowerCase(b):
    if 65 <= ord(b) <= 90:
        b = chr(ord(b) ^ 32)
    return b


def checkIfAlphabet(c):
    if 65 <= ord(c) <= 90:
        return True
    elif 65 <= ord(c) ^ 32 <= 90:
        return True
    else:
        return False


def checkIfDigit(d):
    if 40 <= ord(d) <= 57:
        return True
    else:
        return False


def checkIfSpecialCharacter(e):
    if 32 <= ord(e) <= 47:
        return True
    elif 58 <= ord(e) <= 64:
        return True
    elif 91 <= ord(e) <= 96:
        return True
    elif 123 <= ord(e) <= 126:
        return True
    else:
        return False


# Asks user to enter character to check if it's lower case
lowerCaseLetter = input("Enter a lower case letter to convert to uppercase: ")
print("Upper Case:", toUpperCase(lowerCaseLetter))

# Asks user to enter character to check if it's upper case
upperCaseLetter = input("Enter a upper case letter to convert to lowercase: ")
print("Lower Case:", toLowerCase(upperCaseLetter))


# Asks user to enter character to check if it's in alphabet
alphabetCheck = input("Enter any key to check if its in the alphabet: ")
value = checkIfAlphabet(alphabetCheck)
print(value)

# Asks user to enter character to check if it's a digit
digitCheck = input("Enter any key to check if its a digit: ")
value = checkIfDigit(digitCheck)
print(value)

# Asks user to enter character to check if it's a special character
specialCharacterCheck = input("Enter any key to check if its a Spacial Character: ")
value = checkIfSpecialCharacter(specialCharacterCheck)
print(value)